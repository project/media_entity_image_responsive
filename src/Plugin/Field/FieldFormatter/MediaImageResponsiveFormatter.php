<?php

namespace Drupal\media_entity_image_responsive\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'media_image_responsive' formatter.
 *
 * @FieldFormatter(
 *   id = "media_image_responsive",
 *   label = @Translation("Responsive Image"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MediaImageResponsiveFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;
  /**
   * The responsive image style entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $responsiveImageStyleStorage;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $responsive_image_style_storage
   *   The responsive image style storage.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $responsive_image_style_storage, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->responsiveImageStyleStorage = $responsive_image_style_storage;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity.manager')->getStorage('responsive_image_style'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'responsive_image_style' => '',
      'image_link' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Provide a link to the Responsive Image configuration page.
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Responsive Image Styles'),
      Url::fromRoute('entity.responsive_image_style.collection')
    );
    
    // Build a list of all available image styles.
    $responsive_styles = [];
    foreach ($this->responsiveImageStyleStorage->loadMultiple() as $entity) {
      $responsive_styles[$entity->id()] = $entity->label();
    }
    $element['responsive_image_style'] = [
      '#title' => t('Responsive Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('responsive_image_style'),
      '#options' => $responsive_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer responsive images'),
      ],
    ];
    // Set #empty_option if no default_value or style does not exist.
    if (!$this->getSetting('responsive_image_style') || !isset($responsive_styles[$this->getSetting('responsive_image_style')])) {
      $element['responsive_image_style']['#empty_option'] = t('None (original image)');
    }
    $element['image_link'] = [
      '#title' => t('Link image to'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_link'),
      '#empty_option' => t('Nothing'),
      '#options' => $this->getLinkTypes(),
    ];
    return $element + parent::settingsForm($form, $form_state);
  }

  /**
   * Return list of link type options.
   */
  public function getLinkTypes() {
    return [
      'content' => $this->t('Linked to content'),
      'media' => $this->t('Linked to media entity'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    // Display selected Responsive Style, otherwise provide fallback text.
    if ($this->getSetting('responsive_image_style') && ($entity = $this->responsiveImageStyleStorage->load($this->getSetting('responsive_image_style')))) {
      $summary[] = $this->t('Responsive Image Style: @entity_label', ['@entity_label' => $entity->label()]);
    }
    else {
      $this->t('Responsive Image Style: Not selected');
    }
    $image_link = $this->getSetting('image_link');
    if (isset($this->getLinkTypes()[$image_link])) {
      $summary[] = $this->getLinkTypes()[$image_link];
    }
    else {
      $summary[] = $this->t('No Link');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // Load referenced media entities.  Return empty render array on failure.
    if (!$media_entities = $this->getEntitiesToView($items, $langcode)) {
      return $elements;
    }

    $url = NULL;
    $link_media = FALSE;
    $image_link_setting = $this->getSetting('image_link');

    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting == 'media') {
      $link_media = TRUE;
    }

    $responsive_image_style_setting = $this->getSetting('responsive_image_style');
    // Load entity if responsive_image_style is set.
    $responsive_image_style = $responsive_image_style_setting
      ? $this->responsiveImageStyleStorage->load($responsive_image_style_setting)
      : NULL;

    foreach ($media_entities as $delta => $media_entity) {
      $source_field_name = $media_entity->bundle->entity->type_configuration['source_field'];
      if ($link_media) {
        $url = $media_entity->toUrl();
      }
      // If responsive image style exists, render responsive_image.
      if ($responsive_image_style) {
        $elements[$delta] = [
          '#theme' => 'responsive_image_formatter',
          '#item' => $media_entity->get($source_field_name),
          '#item_attributes' => [],
          '#responsive_image_style_id' => $responsive_image_style_setting,
          '#url' => $url,
        ];
      }
      // If not set, render original image.
      else {
        $elements[$delta] = [
          '#theme' => 'image_formatter',
          '#item' => $media_entity->get($source_field_name),
          '#item_attributes' => [],
          '#image_style' => NULL,
          '#url' => $url,
        ];
      }
      // Add cache dependency from media_entity.
      $this->renderer->addCacheableDependency($elements[$delta], $media_entity);
    }
    // Add cache dependecy from responsive_image_style if defined.
    if ($responsive_image_style) {
      $this->renderer->addCacheableDependency($elements, $responsive_image_style);
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type') == 'media';
  }

  /**
   * {@inheritdoc}
   *
   * FileFormatterBase expects $item to be of type
   * \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in EntityReferenceItem.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies) {
    $changed = parent::onDependencyRemoval($dependencies);
    $responsive_image_style_setting = $this->getSetting('responsive_image_style');
    $responsive_image_style = $responsive_image_style_setting ? $this->responsiveImageStyleStorage->load($responsive_image_style_setting) : NULL;
    // If responsive image removed, update responsive_image_style to null.
    if (!$responsive_image_style) {
      $this->setSetting('responsive_image_style', NULL);
      $changed = TRUE;
    }
    return $changed;
  }

}
