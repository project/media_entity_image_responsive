* MEDIA ENTITY IMAGE RESPONSIVE *
================================

CONTENTS OF THIS FILE
---------------------

  * 01 - Introduction
  * 02 - Requirements
  * 03 - Installation
  * 04 - Configuration
  * 05 - Maintainers


01 - INTRODUCTION
-----------------

The Media Entity Image Responsive module provides a field formatter for
Media Entity Images to display as Responsive Images. This only supports
Media Entity, and does not support core media. It is meant as an interim
solution for supporting Responsive Images within Media Entity. For core
Media, check out Media Responsive Thumbnail.

An example use case would be when you want an media entity reference field
to display using Drupal core's responsive image options.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_entity_image_responsive

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/media_entity_image_responsive


02 - REQUIREMENTS
-----------------

This module requires the following modules:

  * Image (https://www.drupal.org/docs/7/core/modules/image)
  * File (https://www.drupal.org/docs/7/core/modules/file)
  * Field (https://www.drupal.org/docs/7/core/modules/field)
  * Media Entity Image (https://www.drupal.org/project/media_entity_image)


03 - INSTALLATION
-----------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


04 - CONFIGURATION
------------------

  * Add Media Entity Field

    - When adding a field to any entity bundle, choose the Media Entity
    Reference field option and configure this field in any way necessary.

  * Select Responsive Image Display

    - After your Media Entity Reference field has been established, manage
    the display of your entity bundle. Choose, "Responsive Image" for the
    previously added Media Entity field and select the desired responsive
    image configuration.


05 - MAINTAINERS
-----------

 * Michael Lander (michaellander) - https://www.drupal.org/u/michaellander
